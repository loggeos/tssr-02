"""victorhugo URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import include,url
from . import views

urlpatterns = [
    path('admin/', admin.site.urls), 
    path(r'', views.home, name='home'),
    path('cours/', include('cours.urls')),
    #url(r'^accounts/', include('django_registration.backends.activation.urls')),
    url(r'^accounts/', include('django.contrib.auth.urls'))
]
