# Generated by Django 2.1.3 on 2018-12-13 10:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cours', '0006_auto_20181213_0921'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enseignant',
            name='telephone',
            field=models.CharField(blank=True, default='none', max_length=25),
        ),
    ]
