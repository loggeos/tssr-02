# Generated by Django 2.1.3 on 2019-01-07 09:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cours', '0011_auto_20190107_0836'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enseignant',
            name='nom',
            field=models.CharField(blank=True, max_length=30),
        ),
        migrations.AlterField(
            model_name='enseignant',
            name='prenom',
            field=models.CharField(blank=True, max_length=30),
        ),
        migrations.AlterField(
            model_name='enseignant',
            name='telephone',
            field=models.CharField(blank=True, max_length=25),
        ),
    ]
