# Generated by Django 2.1.3 on 2018-11-21 10:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cours', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='evaluation',
            old_name='code_matiere',
            new_name='matiere',
        ),
        migrations.RenameField(
            model_name='evaluation',
            old_name='code_type',
            new_name='type_evaluation',
        ),
    ]
