# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm, Form, modelformset_factory
from crispy_forms.layout import Submit, Layout, Field, Button, ButtonHolder
from crispy_forms.bootstrap import TabHolder, Tab, FormActions, StrictButton
from crispy_forms.helper import FormHelper
from django.forms import Form, ModelForm
from cours.models import *
from django.conf import settings

# next version

class EleveForm(forms.ModelForm):
    class Meta:
        model = eleve
        fields = ('__all__')

    def __init__(self, *args, **kwargs):
        super(EleveForm, self).__init__(*args, **kwargs)    
        self.fields['prenom'].required=False
        self.fields['prenom'].label = "Prénom"
        self.fields['nom'].required=False
        self.fields['nom'].label = "Nom"
        self.fields['date_naissance'].required=False
        self.fields['date_naissance'].label = "Date de naissance"
        self.fields['sexe'].label = "Sexe"
        self.fields['sexe']=forms.ModelChoiceField(queryset=sexe.objects.all())
        self.fields['sexe'].required=False
        self.fields['classe'].label = "Classe"
        self.fields['classe']=forms.ModelChoiceField(queryset=classe.objects.all())
        self.fields['classe'].required=False
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-horizontal'
    helper.form_id = 'user_form'
    helper.label_class = 'col-md-4'
    helper.field_class = 'col-md-6' 
    helper.layout = Layout(
        TabHolder(
            Tab(
                'Contact',
                'prenom',
                'nom',
                'date_naissance',
                'sexe',
                'classe',
            ),
        ),  
        FormActions(
            Submit('save','Enregistrer', css_class="btn-success"),
            Submit('cancel','Annuler', css_class="btn-cancel"),
        ), 
    )


class EnseignantsForm(forms.ModelForm):
    class Meta:
        model = enseignant
        fields = ('__all__')

    def __init__(self, *args, **kwargs):
        super(EnseignantsForm, self).__init__(*args, **kwargs)    
        self.fields['prenom'].required=False
        self.fields['prenom'].label = "Prénom"
        self.fields['nom'].required=False
        self.fields['nom'].label = "Nom"
       
        self.fields['sexe'].label = "Sexe"
        self.fields['sexe']=forms.ModelChoiceField(queryset=sexe.objects.all())
        self.fields['sexe'].required=False
        self.fields['telephone'].label = "Téléphone"
        self.fields['telephone'].required=False
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-horizontal'
    helper.form_id = 'user_form'
    helper.label_class = 'col-md-4'
    helper.field_class = 'col-md-6' 
    helper.layout = Layout(
        TabHolder(
            Tab(
                'Contact',
                'prenom',
                'nom',
                'sexe',
                'telephone',
            ),
        ),  
        FormActions(
            Submit('save','Enregistrer', css_class="btn-success"),
            Submit('cancel','Annuler', css_class="btn-cancel"),
        ), 
    )
