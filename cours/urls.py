"""victorhugo URL Configuration
"""
from django.contrib import admin
from django.conf.urls import include,url
from django.conf.urls.static import static
from django.urls import path
from . import views

app_name = 'cours'

urlpatterns = [
    #path(r'', views.home, name='home'),
    url(r'^matieres_list$', views.matieres_list, name='matieres_list'),
    url(r'^classes_list$', views.classes_list, name='classes_list'),
    url(r'^eleves_list$', views.eleves_list, name='eleves_list'),
    url(r'^presence_list$', views.presence_list, name='presence_list'),
    url(r'^eleve_update/(?P<pk>\d+)$', views.eleve_update, name='eleve_update'),	
    url(r'^eleve_file/(?P<pk>\d+)$', views.eleve_file, name='eleve_file'),	
    url(r'^eleve_add$', views.eleve_add, name='eleve_add'),
    url(r'^enseignants_list$', views.enseignants_list, name='enseignants_list'),
    url(r'^enseignant_update/(?P<pk>\d+)$', views.enseignant_update, name='enseignant_update'),
    url(r'^enseignant_file/(?P<pk>\d+)$', views.enseignant_file, name='enseignant_file'),	
    url(r'^enseignant_add$', views.enseignant_add, name='enseignant_add'),

]
