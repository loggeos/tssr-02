# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response,redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import sexe, matiere, classe, salle, type_evaluation, eleve, evaluation, enseignant, cours, passer_evaluation, enseigner, presence
from .forms import EleveForm, EnseignantsForm

@login_required
def matieres_list(request, template_name='cours/matieres_list.html'):
    current_user = request.user
    lists= matiere.objects.all()
    results_lists=lists.count()
    paginator = Paginator(lists, 20)
    page = request.GET.get('page')
    try:
        docus = paginator.page(page)
    except PageNotAnInteger:
        docus = paginator.page(1)
    except EmptyPage:
        docus = paginator.page(paginator.num_pages)
    return render(request,template_name, {'docus': docus ,'page': page , 'results_lists':results_lists})

@login_required
def classes_list(request, template_name='cours/classes_list.html'):
    current_user = request.user
    lists= classe.objects.all()
    results_lists=lists.count()
    paginator = Paginator(lists, 10)
    page = request.GET.get('page')
    try:
        docus = paginator.page(page)
    except PageNotAnInteger:
        docus = paginator.page(1)
    except EmptyPage:
        docus = paginator.page(paginator.num_pages)
    return render(request,template_name, {'docus': docus ,'page': page , 'results_lists':results_lists})

@login_required
def presence_list(request, template_name='cours/presences.html'):
    current_user = request.user
    lists= presence.objects.all()
    paginator = Paginator(lists, 10)
    page = request.GET.get('page')
    try:
        docus = paginator.page(page)
    except PageNotAnInteger:
        docus = paginator.page(1)
    except EmptyPage:
        docus = paginator.page(paginator.num_pages)
    return render(request,template_name, {'docus': docus ,'page': page })


@login_required
def eleves_list(request, template_name='cours/eleves_list.html'):
    current_user = request.user
    lists= eleve.objects.order_by('nom')
    results_lists=lists.count()
    paginator = Paginator(lists, 20)
    page = request.GET.get('page')
    try:
        docus = paginator.page(page)
    except PageNotAnInteger:
        docus = paginator.page(1)
    except EmptyPage:
        docus = paginator.page(paginator.num_pages)
    return render(request,template_name, {'docus': docus ,'page': page , 'results_lists':results_lists})


@login_required
def eleve_file(request, pk, template_name='cours/eleve_fiche.html'):
	data = eleve.objects.filter(id__exact=pk)
	return render(request, template_name, {'data': data})

@login_required
def eleve_add(request, template_name='cours/eleve_form.html'):
    form = EleveForm(request.POST or None,request.FILES or None)
    if form.is_valid():
        docus = form.save(commit=False)
        docus.user = request.user
        docus.save()
        return redirect('/cours/eleves_list', permanent=True)
    return render(request, template_name, {'form':form})

@login_required
def eleve_update(request, pk, template_name='cours/eleve_form.html'):
    if request.user.is_staff:
        docus= get_object_or_404(eleve, pk=pk)
    else:
        docus= get_object_or_404(eleve, pk=pk, user=request.user)
    form = EleveForm(request.POST or None,request.FILES or None, instance=docus)
    if form.is_valid():
        form.save()
        return redirect('/cours/eleves_list', permanent=True)
    return render(request, template_name, {'form':form})

@login_required
def enseignants_list(request, template_name='cours/enseignants_list.html'):
    current_user = request.user
    lists= enseignant.objects.order_by('nom')
    results_lists=lists.count()
    paginator = Paginator(lists, 20)
    page = request.GET.get('page')
    try:
        docus = paginator.page(page)
    except PageNotAnInteger:
        docus = paginator.page(1)
    except EmptyPage:
        docus = paginator.page(paginator.num_pages)
    return render(request,template_name, {'docus': docus,'page': page , 'results_lists':results_lists})

@login_required
def enseignant_update(request, pk, template_name='cours/enseignant_form.html'):
    if request.user.is_staff:
        docus= get_object_or_404(enseignant, pk=pk)
    else:
        docus= get_object_or_404(enseignant, pk=pk, user=request.user)
    form = EnseignantsForm(request.POST or None,request.FILES or None, instance=docus)
    if form.is_valid():
        form.save()
        return redirect('/cours/enseignants_list', permanent=True)
    return render(request, template_name, {'form':form})

@login_required
def enseignant_file(request, pk, template_name='cours/enseignant_fiche.html'):
	data = enseignant.objects.filter(id__exact=pk)
	return render(request, template_name, {'data': data})

@login_required
def enseignant_add(request, template_name='cours/enseignant_form.html'):
    form = EnseignantsForm(request.POST or None,request.FILES or None)
    if form.is_valid():
        docus = form.save(commit=False)
        docus.user = request.user
        docus.save()
        return redirect('/cours/enseignants_list', permanent=True)
    return render(request, template_name, {'form':form})





