# -*- coding: utf-8 -*-
from django.db import models
from django.urls import reverse
import datetime

# Create your models here.


class sexe(models.Model):
    libelle_sexe = models.CharField(max_length=1, blank=True)
    class Meta:
    	verbose_name = 'sexe'
    	verbose_name_plural = 'sexe'
    def __str__(self):
        return self.libelle_sexe

class matiere(models.Model):
    libelle_matiere = models.CharField(max_length=30, blank=True)
    class Meta:
    	verbose_name = 'matière'
    	verbose_name_plural = 'matières'
    def __str__(self):
        return self.libelle_matiere
    def get_absolute_url(self):
        return reverse('matières_detail', args=[str(self.id)])

class classe(models.Model):
    libelle_classe = models.CharField(max_length=10, blank=True)
    class Meta:
    	verbose_name = 'classe'
    	verbose_name_plural = 'classes'
    def __str__(self):
        return self.libelle_classe
    def get_absolute_url(self):
        return reverse('classe_detail', args=[str(self.id)])
 

class salle(models.Model):
    libelle_salle = models.CharField(max_length=10, blank=True)
    capacite = models.IntegerField(blank=True, default=0)
    localisation = models.CharField(max_length=30, blank=True)
    class Meta:
    	verbose_name = 'salle'
    	verbose_name_plural = 'salles'
    def __str__(self):
        return self.libelle_salle
    def get_absolute_url(self):
        return reverse('salle_detail', args=[str(self.id)])

class type_evaluation(models.Model):
    libelle_type_evaluation = models.CharField(max_length=30, blank=True)
    class Meta:
    	verbose_name = "type d'évaluation"
    	verbose_name_plural = "types d'évaluation"
    def __str__(self):
        return self.libelle_type_evaluation

class eleve(models.Model):
	nom = models.CharField(max_length=30, blank=True)
	prenom = models.CharField(max_length=30, blank=True)
	date_naissance = models.DateField(blank=True, null=True)
	sexe = models.ForeignKey('sexe', on_delete=models.CASCADE, null=True )
	classe = models.ForeignKey('classe', on_delete=models.CASCADE, blank=True, null=True)
	class Meta:
		verbose_name = 'élève'
		verbose_name_plural = 'élèves'
		ordering = ['-nom']
	def __str__(self):
		return '%s %s' % (self.nom, self.prenom)
	def get_absolute_url(self):
		return reverse('élève_detail', args=[str(self.id)])

class evaluation(models.Model):
	libelle_evaluation = models.CharField(max_length=10, blank=True)
	date_evaluation = models.DateTimeField()
	coefficient = models.PositiveSmallIntegerField()
	type_evaluation = models.ForeignKey('type_evaluation', on_delete=models.CASCADE )
	matiere = models.ForeignKey('matiere', on_delete=models.CASCADE)
	class Meta:
		verbose_name = 'évaluation'
		verbose_name_plural = 'évaluations'
	def __str__(self):
		return self.libelle_evaluation

class enseignant(models.Model):
	nom = models.CharField(max_length=30, blank=True,)
	prenom = models.CharField(max_length=30, blank=True)
	sexe = models.ForeignKey('sexe', on_delete=models.CASCADE, null=True)
	telephone = models.CharField(max_length=25, blank=True)
	class Meta:
		verbose_name ='enseignant'
		verbose_name_plural = 'enseignants'
		ordering = ['nom']
	def __str__(self):
		return '%s %s' % (self.nom, self.prenom)
	def get_absolute_url(self):
		return reverse('enseignant_detail', args=[str(self.id)])

class cours(models.Model):
	code_court = models.CharField(max_length=5)
	description = models.CharField(max_length=30)
	date = models.DateField()
	heure_debut = models.TimeField()
	heure_fin = models.TimeField()
	salle = models.ForeignKey('salle', on_delete=models.CASCADE, null=True)
	classe = models.ForeignKey('classe', on_delete=models.CASCADE, null=True)
	enseignant = models.ForeignKey('enseignant', on_delete=models.CASCADE, null=True)
	class Meta:
		indexes = [models.Index(fields=['code_court'])]
		verbose_name = 'cours'
		verbose_name_plural = 'cours'
	def __str__(self):
		return '%s %s %s %s %s' % (self.date, self.salle, self.enseignant, self.classe, self.description)
	def get_absolute_url(self):
		return reverse('cours_detail', args=[str(self.id)])

class passer_evaluation(models.Model):
	note = models.FloatField()
	present = models.BooleanField(default=False)
	evaluation = models.ForeignKey('evaluation', on_delete=models.CASCADE)
	eleve = models.ForeignKey('eleve', on_delete=models.CASCADE)
	class Meta:
		verbose_name = 'passer évaluation'
		verbose_name_plural = 'passe évaluation'
	def __str__(self):
		return '%s %s' % (self.eleve, self.note)

class enseigner(models.Model):
	matiere = models.ForeignKey('matiere', on_delete=models.CASCADE)
	enseignant = models.ForeignKey('enseignant', on_delete=models.CASCADE)
	class Meta:
		verbose_name = 'enseigner'
		verbose_name_plural = 'enseigner'
	def __str__(self):
		return '%s %s' % (self.enseignant, self.matiere)

class presence(models.Model):
	date = models.DateField('Date de présence')
	eleve = models.ForeignKey('eleve', on_delete=models.CASCADE)
	present = models.BooleanField('présent', default=True)
	class Meta:
		verbose_name = 'présence'
		verbose_name_plural = 'présence'
	def __str__(self):
		return '%s %s' % (self.eleve, self.present)
